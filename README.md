# Morningview.cl Songs Lists

Esta es una lista de canciones para la radio morningview.cl. Se crea este repositorio con el fin de ir recolectando links, estilos, artistas y noticias relacionadas para la generaci�n de contenido del sitio/radio.

## Listas

 - [Lista por artistas](#artistas)
 - [Lista por g�nero](#genero)
 - [Lista de canales ](#canales)

### Lista por artista

 - A
 - B
 - C
    - Childish Gambino: https://www.youtube.com/watch?v=UfQHEpf2q8k
 - D
 - E
 - F
    - Fortunate Youth: https://www.youtube.com/watch?v=1xP1rbYhQEc
 - G
 - H
 - I
 - J
 - K
 - L
 - M
    - Mac Miller: https://www.youtube.com/watch?v=MqcOWCecf5s
    - Mac Miller: https://www.youtube.com/watch?v=QrR_gm6RqCo
 - N
 - �
 - O
    - Ocean alley: https://www.youtube.com/watch?v=ubAbewMTUUw
 - P
 - Q
 - R
 - S
	 - Stephen Marley: https://www.youtube.com/watch?v=Pl_dfrsnuXY
 - T
	 - The Roots: https://www.youtube.com/watch?v=nWhIayQL8YY
 - U
 - V
 - W
 - X
 - Y
 - Z
## Lista de canales

 - NPM Tiny desk: https://www.youtube.com/channel/UC4eYXhJI4-7wSWc8UNRwD4A
 - Sugar Shack Sessions: https://www.youtube.com/channel/UCSHfOL-Ke32Qi7Sn9_BGTqg 
 - Triple J: https://www.youtube.com/channel/UCd2KNtfphz8HvYzM4pwtHmg
 